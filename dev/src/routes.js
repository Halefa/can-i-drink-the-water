// routes.js

import Home from "./components/Home.vue";
import CountryPicker from "./components/CountryPicker.vue";

const routes = [
  {
    path: "/CountryPicker",
    component: CountryPicker
  },
  {
    path: "/:countryCode?",
    component: Home
  }
];

export default routes;
