import Vue from "vue";
import App from "./App.vue";
import VueRouter from "vue-router";

require("./assets/emoji.css");

Vue.use(VueRouter);

import routes from "./routes";

Vue.config.productionTip = false;

const router = new VueRouter({
  mode: "history",
  base: "/",
  routes // short for `routes: routes`
});

new Vue({
  router,
  render: h => h(App)
}).$mount("#app");
