const countries = [
  {
    listCountry: "Afghanistan",
    quality: false,
    warning: false,
    source:
      "https://wwwnc.cdc.gov/travel/destinations/traveler/none/afghanistan",
    flag: "af"
  },
  {
    listCountry: "Albania",
    quality: false,
    warning: false,
    source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/albania",
    flag: "al"
  },
  {
    listCountry: "Algeria",
    quality: false,
    warning: false,
    source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/algeria",
    flag: "dz"
  },
  {
    listCountry: "American Samoa",
    quality: false,
    warning: false,
    source:
      "https://wwwnc.cdc.gov/travel/destinations/traveler/none/american-samoa",
      flag: "as"
  },
  {
    listCountry: "Denmark",
    quality: false,
    warning: false,
    source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/andorra",
    flag: "ad"
  }
];

// export default countries;