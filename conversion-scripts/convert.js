// require the file system core module
var fs = require("fs");
// Read the csv file
var contents = fs.readFileSync("data_v3.csv", "utf8");

// Send the data from the csv file to the parser
const parse = require("csv-parse/lib/sync");

const records = parse(contents, {
  columns: true,
  skip_empty_lines: true,
  delimiter: ";"
});

records.forEach((row, index, array) => {
  if (row.Drinkable.substring(0, 2) === "No") {
    row.Drinkable = false;
  } else {
    row.Drinkable = true;
  }
  if (row.Warning.substring(0, 3) === "You") {
    row.Warning = true;
  } else {
    row.Warning = false;
  }
  let tempFlag = row.Flag.split("-");
  row.countryCode = tempFlag[2];

  if (row.Source.substring(0, 28) === "https://wwwnc.cdc.gov/travel") {
    row.shortSource = "https://wwwnc.cdc.gov/travel";
  } else if (row.Source.substring(0, 26) === "https://www.tripsavvy.com/") {
    row.shortSource = "https://www.tripsavvy.com/";
  } else {
    row.shortSource = "https://wikitravel.org/";
  }
});

var duplicateCountryCodes = {};

const filteredRecords = records.filter(function(currentObject) {
  if (currentObject.countryCode in duplicateCountryCodes) {
    return false;
  } else {
    duplicateCountryCodes[currentObject.countryCode] = true;
    return true;
  }
});

console.log(records.length);
console.log(filteredRecords.length);
console.log(duplicateCountryCodes);

// console.log(records);

// Converting json to string
var json = JSON.stringify(filteredRecords);

// Saving data to file
fs.writeFileSync("data_v4_filtered.json", json);
