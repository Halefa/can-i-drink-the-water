[
  {
    listCountry: "Afghanistan",
    Drinkable: false,
    Warning: false,
    Source:
      "https://wwwnc.cdc.gov/travel/destinations/traveler/none/afghanistan",
    Flag: "em-flag-af",
    countryCode: "af",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Albania",
    Drinkable: false,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/albania",
    Flag: "em-flag-al",
    countryCode: "al",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Algeria",
    Drinkable: false,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/algeria",
    Flag: "em-flag-dz",
    countryCode: "dz",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "American Samoa",
    Drinkable: false,
    Warning: false,
    Source:
      "https://wwwnc.cdc.gov/travel/destinations/traveler/none/american-samoa",
    Flag: "em-flag-as",
    countryCode: "as",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Andorra",
    Drinkable: true,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/andorra",
    Flag: "em-flag-ad",
    countryCode: "ad",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Angola",
    Drinkable: false,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/angola",
    Flag: "em-flag-ao",
    countryCode: "ao",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Anguilla",
    Drinkable: false,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/anguilla",
    Flag: "em-flag-ai",
    countryCode: "ai",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Antigua and Barbuda",
    Drinkable: false,
    Warning: false,
    Source:
      "https://wwwnc.cdc.gov/travel/destinations/traveler/none/antigua-and-barbuda",
    Flag: "em-flag-ag",
    countryCode: "ag",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Argentina",
    Drinkable: false,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/argentina",
    Flag: "em-flag-ar",
    countryCode: "ar",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Armenia",
    Drinkable: false,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/armenia",
    Flag: "em-flag-am",
    countryCode: "am",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Aruba",
    Drinkable: true,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/aruba",
    Flag: "em-flag-aw",
    countryCode: "aw",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Australia",
    Drinkable: true,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/australia",
    Flag: "em-flag-au",
    countryCode: "au",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Austria",
    Drinkable: true,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/austria",
    Flag: "em-flag-at",
    countryCode: "at",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Azerbaijan",
    Drinkable: false,
    Warning: false,
    Source:
      "https://wwwnc.cdc.gov/travel/destinations/traveler/none/azerbaijan",
    Flag: "em-flag-az",
    countryCode: "az",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Azores",
    Drinkable: false,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/azores",
    Flag: "em-flag-pt",
    countryCode: "pt",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Bahamas, The",
    Drinkable: false,
    Warning: false,
    Source:
      "https://wwwnc.cdc.gov/travel/destinations/traveler/none/the-bahamas",
    Flag: "em-flag-bs",
    countryCode: "bs",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Bahrain",
    Drinkable: true,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/bahrain",
    Flag: "em-flag-bh",
    countryCode: "bh",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Bangladesh",
    Drinkable: false,
    Warning: false,
    Source:
      "https://wwwnc.cdc.gov/travel/destinations/traveler/none/bangladesh",
    Flag: "em-flag-bd",
    countryCode: "bd",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Barbados",
    Drinkable: false,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/barbados",
    Flag: "em-flag-bb",
    countryCode: "bb",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Belarus",
    Drinkable: false,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/belarus",
    Flag: "em-flag-by",
    countryCode: "by",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Belgium",
    Drinkable: true,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/belgium",
    Flag: "em-flag-be",
    countryCode: "be",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Belize",
    Drinkable: false,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/belize",
    Flag: "em-flag-bz",
    countryCode: "bz",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Benin",
    Drinkable: false,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/benin",
    Flag: "em-flag-bj",
    countryCode: "bj",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Bermuda",
    Drinkable: true,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/bermuda",
    Flag: "em-flag-bm",
    countryCode: "bm",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Bhutan",
    Drinkable: false,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/bhutan",
    Flag: "em-flag-bt",
    countryCode: "bt",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Bolivia",
    Drinkable: false,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/bolivia",
    Flag: "em-flag-bo",
    countryCode: "bo",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Bonaire",
    Drinkable: false,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/bonaire",
    Flag: "em-flag-bq",
    countryCode: "bq",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Bosnia and Herzegovina",
    Drinkable: false,
    Warning: false,
    Source:
      "https://wwwnc.cdc.gov/travel/destinations/traveler/none/bosnia-and-herzegovina",
    Flag: "em-flag-ba",
    countryCode: "ba",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Botswana",
    Drinkable: false,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/botswana",
    Flag: "em-flag-bw",
    countryCode: "bw",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Brazil",
    Drinkable: false,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/brazil",
    Flag: "em-flag-br",
    countryCode: "br",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "British Indian Ocean Territory",
    Drinkable: false,
    Warning: false,
    Source:
      "https://wwwnc.cdc.gov/travel/destinations/traveler/none/british-indian-ocean-territory",
    Flag: "em-flag-io",
    countryCode: "io",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Brunei",
    Drinkable: false,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/brunei",
    Flag: "em-flag-bn",
    countryCode: "bn",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Bulgaria",
    Drinkable: false,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/bulgaria",
    Flag: "em-flag-bg",
    countryCode: "bg",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Burkina Faso",
    Drinkable: false,
    Warning: false,
    Source:
      "https://wwwnc.cdc.gov/travel/destinations/traveler/none/burkina-faso",
    Flag: "em-flag-bf",
    countryCode: "bf",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Burma (Myanmar)",
    Drinkable: false,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/burma",
    Flag: "em-flag-mm",
    countryCode: "mm",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Burundi",
    Drinkable: false,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/burundi",
    Flag: "em-flag-bi",
    countryCode: "bi",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Cambodia",
    Drinkable: false,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/cambodia",
    Flag: "em-flag-kh",
    countryCode: "kh",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Cameroon",
    Drinkable: false,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/cameroon",
    Flag: "em-flag-cm",
    countryCode: "cm",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Canada",
    Drinkable: true,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/canada",
    Flag: "em-flag-ca",
    countryCode: "ca",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Cape Verde",
    Drinkable: false,
    Warning: false,
    Source:
      "https://wwwnc.cdc.gov/travel/destinations/traveler/none/cape-verde",
    Flag: "em-flag-cv",
    countryCode: "cv",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Cayman Islands (U.K.)",
    Drinkable: false,
    Warning: false,
    Source:
      "https://wwwnc.cdc.gov/travel/destinations/traveler/none/cayman-islands",
    Flag: "em-flag-ky",
    countryCode: "ky",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Central African Republic",
    Drinkable: false,
    Warning: false,
    Source:
      "https://wwwnc.cdc.gov/travel/destinations/traveler/none/central-african-republic",
    Flag: "em-flag-cf",
    countryCode: "cf",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Chad",
    Drinkable: false,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/chad",
    Flag: "em-flag-td",
    countryCode: "td",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Chile",
    Drinkable: true,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/chile",
    Flag: "em-flag-cl",
    countryCode: "cl",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "China",
    Drinkable: false,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/china",
    Flag: "em-flag-cn",
    countryCode: "cn",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Christmas Island",
    Drinkable: false,
    Warning: false,
    Source:
      "https://wwwnc.cdc.gov/travel/destinations/traveler/none/christmas-island",
    Flag: "em-flag-cx",
    countryCode: "cx",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Cocos (Keeling) Islands",
    Drinkable: false,
    Warning: false,
    Source:
      "https://wwwnc.cdc.gov/travel/destinations/traveler/none/cocos-islands",
    Flag: "em-flag-cc",
    countryCode: "cc",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Colombia",
    Drinkable: false,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/colombia",
    Flag: "em-flag-co",
    countryCode: "co",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Comoros",
    Drinkable: false,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/comoros",
    Flag: "em-flag-km",
    countryCode: "km",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Congo, Republic of the",
    Drinkable: false,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/congo",
    Flag: "em-flag-cg",
    countryCode: "cg",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Cook Islands",
    Drinkable: true,
    Warning: false,
    Source:
      "https://wwwnc.cdc.gov/travel/destinations/traveler/none/cook-islands",
    Flag: "em-flag-ck",
    countryCode: "ck",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Costa Rica",
    Drinkable: true,
    Warning: false,
    Source:
      "https://wwwnc.cdc.gov/travel/destinations/traveler/none/costa-rica",
    Flag: "em-flag-cr",
    countryCode: "cr",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Croatia",
    Drinkable: true,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/croatia",
    Flag: "em-flag-hr",
    countryCode: "hr",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Cuba",
    Drinkable: false,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/cuba",
    Flag: "em-flag-cu",
    countryCode: "cu",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Curaçao",
    Drinkable: true,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/curacao",
    Flag: "em-flag-cw",
    countryCode: "cw",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Cyprus",
    Drinkable: false,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/cyprus",
    Flag: "em-flag-cy",
    countryCode: "cy",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Czech Republic",
    Drinkable: true,
    Warning: false,
    Source:
      "https://wwwnc.cdc.gov/travel/destinations/traveler/none/czech-republic",
    Flag: "em-flag-cz",
    countryCode: "cz",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Côte d'Ivoire",
    Drinkable: false,
    Warning: false,
    Source:
      "https://wwwnc.cdc.gov/travel/destinations/traveler/none/ivory-coast",
    Flag: "em-flag-ci",
    countryCode: "ci",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Democratic Republic of the Congo",
    Drinkable: false,
    Warning: false,
    Source:
      "https://wwwnc.cdc.gov/travel/destinations/traveler/none/democratic-republic-of-congo",
    Flag: "em-flag-cd",
    countryCode: "cd",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Denmark",
    Drinkable: true,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/denmark",
    Flag: "em-flag-dk",
    countryCode: "dk",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Djibouti",
    Drinkable: false,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/djibouti",
    Flag: "em-flag-dj",
    countryCode: "dj",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Dominica",
    Drinkable: false,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/dominica",
    Flag: "em-flag-dm",
    countryCode: "dm",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Dominican Republic",
    Drinkable: false,
    Warning: false,
    Source:
      "https://wwwnc.cdc.gov/travel/destinations/traveler/none/dominican-republic",
    Flag: "em-flag-do",
    countryCode: "do",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Ecuador",
    Drinkable: false,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/ecuador",
    Flag: "em-flag-ec",
    countryCode: "ec",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Egypt",
    Drinkable: false,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/egypt",
    Flag: "em-flag-eg",
    countryCode: "eg",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "El Salvador",
    Drinkable: false,
    Warning: false,
    Source:
      "https://wwwnc.cdc.gov/travel/destinations/traveler/none/el-salvador",
    Flag: "em-flag-sv",
    countryCode: "sv",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "United Kingdom",
    Drinkable: true,
    Warning: false,
    Source:
      "https://wwwnc.cdc.gov/travel/destinations/traveler/none/united-kingdom",
    Flag: "em-flag-gb",
    countryCode: "gb",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Equatorial Guinea",
    Drinkable: false,
    Warning: false,
    Source:
      "https://wwwnc.cdc.gov/travel/destinations/traveler/none/equatorial-guinea",
    Flag: "em-flag-gq",
    countryCode: "gq",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Eritrea",
    Drinkable: false,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/eritrea",
    Flag: "em-flag-er",
    countryCode: "er",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Estonia",
    Drinkable: true,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/estonia",
    Flag: "em-flag-ee",
    countryCode: "ee",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Ethiopia",
    Drinkable: false,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/ethiopia",
    Flag: "em-flag-et",
    countryCode: "et",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Falkland Islands (Islas Malvinas)",
    Drinkable: false,
    Warning: false,
    Source:
      "https://wwwnc.cdc.gov/travel/destinations/traveler/none/falkland-islands",
    Flag: "em-flag-fk",
    countryCode: "fk",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Faroe Islands",
    Drinkable: false,
    Warning: false,
    Source:
      "https://wwwnc.cdc.gov/travel/destinations/traveler/none/faroe-island",
    Flag: "em-flag-fo",
    countryCode: "fo",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Fiji",
    Drinkable: false,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/fiji",
    Flag: "em-flag-fj",
    countryCode: "fj",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Finland",
    Drinkable: true,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/finland",
    Flag: "em-flag-fi",
    countryCode: "fi",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "France",
    Drinkable: true,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/france",
    Flag: "em-flag-fr",
    countryCode: "fr",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "French Guiana",
    Drinkable: false,
    Warning: false,
    Source:
      "https://wwwnc.cdc.gov/travel/destinations/traveler/none/french-guiana",
    Flag: "em-flag-gf",
    countryCode: "gf",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "French Polynesia",
    Drinkable: false,
    Warning: false,
    Source:
      "https://wwwnc.cdc.gov/travel/destinations/traveler/none/french-polynesia",
    Flag: "em-flag-pf",
    countryCode: "pf",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Gabon",
    Drinkable: false,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/gabon",
    Flag: "em-flag-ga",
    countryCode: "ga",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Gambia, The",
    Drinkable: false,
    Warning: false,
    Source:
      "https://wwwnc.cdc.gov/travel/destinations/traveler/none/the-gambia",
    Flag: "em-flag-gm",
    countryCode: "gm",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Georgia",
    Drinkable: false,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/georgia",
    Flag: "em-flag-ge",
    countryCode: "ge",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Germany",
    Drinkable: true,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/germany",
    Flag: "em-flag-de",
    countryCode: "de",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Ghana",
    Drinkable: false,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/ghana",
    Flag: "em-flag-gh",
    countryCode: "gh",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Gibraltar",
    Drinkable: false,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/gibraltar",
    Flag: "em-flag-gi",
    countryCode: "gi",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Greece",
    Drinkable: true,
    Warning: false,
    Source: "https://www.tripsavvy.com/tap-water-in-europe-3150039",
    Flag: "em-flag-gr",
    countryCode: "gr",
    shortSource: "https://www.tripsavvy.com/"
  },
  {
    listCountry: "Greenland",
    Drinkable: true,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/greenland",
    Flag: "em-flag-gl",
    countryCode: "gl",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Grenada",
    Drinkable: false,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/grenada",
    Flag: "em-flag-gd",
    countryCode: "gd",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Guadeloupe",
    Drinkable: true,
    Warning: false,
    Source:
      "https://wwwnc.cdc.gov/travel/destinations/traveler/none/guadeloupe",
    Flag: "em-flag-gp",
    countryCode: "gp",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Guam",
    Drinkable: false,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/guam",
    Flag: "em-flag-gu",
    countryCode: "gu",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Guatemala",
    Drinkable: false,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/guatemala",
    Flag: "em-flag-gt",
    countryCode: "gt",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Guinea",
    Drinkable: false,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/guinea",
    Flag: "em-flag-gn",
    countryCode: "gn",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Guinea-Bissau",
    Drinkable: false,
    Warning: false,
    Source:
      "https://wwwnc.cdc.gov/travel/destinations/traveler/none/guinea-bissau",
    Flag: "em-flag-jm",
    countryCode: "jm",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Haiti",
    Drinkable: false,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/haiti",
    Flag: "em-flag-ht",
    countryCode: "ht",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Honduras",
    Drinkable: false,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/honduras",
    Flag: "em-flag-hn",
    countryCode: "hn",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Hong Kong",
    Drinkable: false,
    Warning: false,
    Source:
      "https://wwwnc.cdc.gov/travel/destinations/traveler/none/hong-kong-sar",
    Flag: "em-flag-hk",
    countryCode: "hk",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Hungary",
    Drinkable: true,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/hungary",
    Flag: "em-flag-hu",
    countryCode: "hu",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Iceland",
    Drinkable: true,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/iceland",
    Flag: "em-flag-is",
    countryCode: "is",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "India",
    Drinkable: false,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/india",
    Flag: "em-flag-in",
    countryCode: "in",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Indonesia",
    Drinkable: false,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/indonesia",
    Flag: "em-flag-ic",
    countryCode: "ic",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Iran",
    Drinkable: false,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/iran",
    Flag: "em-flag-ir",
    countryCode: "ir",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Iraq",
    Drinkable: false,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/iraq",
    Flag: "em-flag-iq",
    countryCode: "iq",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Ireland",
    Drinkable: true,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/ireland",
    Flag: "em-flag-ie",
    countryCode: "ie",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Isle of Man",
    Drinkable: true,
    Warning: false,
    Source:
      "https://wwwnc.cdc.gov/travel/destinations/traveler/none/united-kingdom",
    Flag: "em-flag-im",
    countryCode: "im",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Israel, West Bank, Gaza",
    Drinkable: true,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/israel",
    Flag: "em-flag-il",
    countryCode: "il",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Italy",
    Drinkable: true,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/italy",
    Flag: "em-flag-it",
    countryCode: "it",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Japan",
    Drinkable: false,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/japan",
    Flag: "em-flag-jp",
    countryCode: "jp",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Jersey",
    Drinkable: true,
    Warning: false,
    Source:
      "https://wwwnc.cdc.gov/travel/destinations/traveler/none/united-kingdom",
    Flag: "em-flag-je",
    countryCode: "je",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Jordan",
    Drinkable: false,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/jordan",
    Flag: "em-flag-jo",
    countryCode: "jo",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Kazakhstan",
    Drinkable: false,
    Warning: false,
    Source:
      "https://wwwnc.cdc.gov/travel/destinations/traveler/none/kazakhstan",
    Flag: "em-flag-kz",
    countryCode: "kz",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Kenya",
    Drinkable: false,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/kenya",
    Flag: "em-flag-ke",
    countryCode: "ke",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Kiribati",
    Drinkable: false,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/kiribati",
    Flag: "em-flag-ki",
    countryCode: "ki",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Kosovo",
    Drinkable: false,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/kosovo",
    Flag: "em-flag-xk",
    countryCode: "xk",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Kuwait",
    Drinkable: true,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/kuwait",
    Flag: "em-flag-kw",
    countryCode: "kw",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Kyrgyzstan",
    Drinkable: false,
    Warning: false,
    Source:
      "https://wwwnc.cdc.gov/travel/destinations/traveler/none/kyrgyzstan",
    Flag: "em-flag-kg",
    countryCode: "kg",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Laos",
    Drinkable: false,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/laos",
    Flag: "em-flag-la",
    countryCode: "la",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Latvia",
    Drinkable: true,
    Warning: false,
    Source: "https://wikitravel.org/en/Latvia",
    Flag: "em-flag-lv",
    countryCode: "lv",
    shortSource: "https://wikitravel.org/"
  },
  {
    listCountry: "Lebanon",
    Drinkable: false,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/lebanon",
    Flag: "em-flag-lb",
    countryCode: "lb",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Lesotho",
    Drinkable: false,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/lesotho",
    Flag: "em-flag-ls",
    countryCode: "ls",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Liberia",
    Drinkable: false,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/liberia",
    Flag: "em-flag-lr",
    countryCode: "lr",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Libya",
    Drinkable: false,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/libya",
    Flag: "em-flag-ly",
    countryCode: "ly",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Liechtenstein",
    Drinkable: true,
    Warning: false,
    Source:
      "https://wwwnc.cdc.gov/travel/destinations/traveler/none/liechtenstein",
    Flag: "em-flag-li",
    countryCode: "li",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Lithuania",
    Drinkable: true,
    Warning: false,
    Source: "https://www.tripsavvy.com/tap-water-in-europe-3150039",
    Flag: "em-flag-lt",
    countryCode: "lt",
    shortSource: "https://www.tripsavvy.com/"
  },
  {
    listCountry: "Luxembourg",
    Drinkable: true,
    Warning: false,
    Source:
      "https://wwwnc.cdc.gov/travel/destinations/traveler/none/luxembourg",
    Flag: "em-flag-lu",
    countryCode: "lu",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Macau",
    Drinkable: false,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/macau-sar",
    Flag: "em-flag-mo",
    countryCode: "mo",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Macedonia",
    Drinkable: true,
    Warning: false,
    Source: "https://www.tripsavvy.com/tap-water-in-europe-3150039",
    Flag: "em-flag-mk",
    countryCode: "mk",
    shortSource: "https://www.tripsavvy.com/"
  },
  {
    listCountry: "Madagascar",
    Drinkable: false,
    Warning: false,
    Source:
      "https://wwwnc.cdc.gov/travel/destinations/traveler/none/madagascar",
    Flag: "em-flag-mg",
    countryCode: "mg",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Malawi",
    Drinkable: false,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/malawi",
    Flag: "em-flag-mw",
    countryCode: "mw",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Malaysia",
    Drinkable: false,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/malaysia",
    Flag: "em-flag-my",
    countryCode: "my",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Maldives",
    Drinkable: false,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/maldives",
    Flag: "em-flag-mv",
    countryCode: "mv",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Mali",
    Drinkable: false,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/mali",
    Flag: "em-flag-ml",
    countryCode: "ml",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Malta",
    Drinkable: true,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/malta",
    Flag: "em-flag-mt",
    countryCode: "mt",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Marshall Islands",
    Drinkable: false,
    Warning: false,
    Source:
      "https://wwwnc.cdc.gov/travel/destinations/traveler/none/marshall-islands",
    Flag: "em-flag-mh",
    countryCode: "mh",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Martinique",
    Drinkable: true,
    Warning: false,
    Source:
      "https://wwwnc.cdc.gov/travel/destinations/traveler/none/martinique",
    Flag: "em-flag-mq",
    countryCode: "mq",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Mauritania",
    Drinkable: false,
    Warning: false,
    Source:
      "https://wwwnc.cdc.gov/travel/destinations/traveler/none/mauritania",
    Flag: "em-flag-mr",
    countryCode: "mr",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Mauritius",
    Drinkable: false,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/mauritius",
    Flag: "em-flag-mu",
    countryCode: "mu",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Mayotte",
    Drinkable: false,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/mayotte",
    Flag: "em-flag-yt",
    countryCode: "yt",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Mexico",
    Drinkable: false,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/mexico",
    Flag: "em-flag-mx",
    countryCode: "mx",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Micronesia, Federated State of",
    Drinkable: false,
    Warning: false,
    Source:
      "https://wwwnc.cdc.gov/travel/destinations/traveler/none/micronesia",
    Flag: "em-flag-fm",
    countryCode: "fm",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Moldova",
    Drinkable: false,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/moldova",
    Flag: "em-flag-md",
    countryCode: "md",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Monaco",
    Drinkable: true,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/monaco",
    Flag: "em-flag-ma",
    countryCode: "ma",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Mongolia",
    Drinkable: false,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/mongolia",
    Flag: "em-flag-mn",
    countryCode: "mn",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Montenegro",
    Drinkable: false,
    Warning: false,
    Source:
      "https://wwwnc.cdc.gov/travel/destinations/traveler/none/montenegro",
    Flag: "em-flag-me",
    countryCode: "me",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Mozambique",
    Drinkable: false,
    Warning: false,
    Source:
      "https://wwwnc.cdc.gov/travel/destinations/traveler/none/mozambique",
    Flag: "em-flag-mz",
    countryCode: "mz",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Namibia",
    Drinkable: false,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/namibia",
    Flag: "em-flag-na",
    countryCode: "na",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Nauru",
    Drinkable: false,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/nauru",
    Flag: "em-flag-nr",
    countryCode: "nr",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Nepal",
    Drinkable: false,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/nepal",
    Flag: "em-flag-np",
    countryCode: "np",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Netherlands",
    Drinkable: true,
    Warning: false,
    Source:
      "https://wwwnc.cdc.gov/travel/destinations/traveler/none/netherlands",
    Flag: "em-flag-nl",
    countryCode: "nl",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "New Caledonia",
    Drinkable: true,
    Warning: false,
    Source:
      "https://wwwnc.cdc.gov/travel/destinations/traveler/none/new-caledonia",
    Flag: "em-flag-nc",
    countryCode: "nc",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "New Zealand",
    Drinkable: true,
    Warning: false,
    Source:
      "https://wwwnc.cdc.gov/travel/destinations/traveler/none/new-zealand",
    Flag: "em-flag-nz",
    countryCode: "nz",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Nicaragua",
    Drinkable: false,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/nicaragua",
    Flag: "em-flag-ni",
    countryCode: "ni",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Niger",
    Drinkable: false,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/niger",
    Flag: "em-flag-ne",
    countryCode: "ne",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Nigeria",
    Drinkable: false,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/nigeria",
    Flag: "em-flag-ng",
    countryCode: "ng",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Niue (New Zealand)",
    Drinkable: false,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/niue",
    Flag: "em-flag-nu",
    countryCode: "nu",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Norfolk Island",
    Drinkable: false,
    Warning: false,
    Source:
      "https://wwwnc.cdc.gov/travel/destinations/traveler/none/norfolk-island",
    Flag: "em-flag-nf",
    countryCode: "nf",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "North Korea",
    Drinkable: false,
    Warning: false,
    Source:
      "https://wwwnc.cdc.gov/travel/destinations/traveler/none/north-korea",
    Flag: "em-flag-kp",
    countryCode: "kp",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Oman",
    Drinkable: false,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/oman",
    Flag: "em-flag-om",
    countryCode: "om",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Pakistan",
    Drinkable: false,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/pakistan",
    Flag: "em-flag-pk",
    countryCode: "pk",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Palau",
    Drinkable: true,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/palau",
    Flag: "em-flag-pw",
    countryCode: "pw",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Panama",
    Drinkable: false,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/panama",
    Flag: "em-flag-pa",
    countryCode: "pa",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Papua New Guinea",
    Drinkable: false,
    Warning: false,
    Source:
      "https://wwwnc.cdc.gov/travel/destinations/traveler/none/papua-new-guinea",
    Flag: "em-flag-pg",
    countryCode: "pg",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Paraguay",
    Drinkable: false,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/paraguay",
    Flag: "em-flag-py",
    countryCode: "py",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Peru",
    Drinkable: false,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/peru",
    Flag: "em-flag-pe",
    countryCode: "pe",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Philippines",
    Drinkable: false,
    Warning: false,
    Source:
      "https://wwwnc.cdc.gov/travel/destinations/traveler/none/philippines",
    Flag: "em-flag-ph",
    countryCode: "ph",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Pitcairn Islands",
    Drinkable: false,
    Warning: false,
    Source:
      "https://wwwnc.cdc.gov/travel/destinations/traveler/none/pitcairn-islands",
    Flag: "em-flag-pn",
    countryCode: "pn",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Poland",
    Drinkable: true,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/poland",
    Flag: "em-flag-pl",
    countryCode: "pl",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Puerto Rico",
    Drinkable: true,
    Warning: false,
    Source:
      "https://wwwnc.cdc.gov/travel/destinations/traveler/none/puerto-rico",
    Flag: "em-flag-pr",
    countryCode: "pr",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Qatar",
    Drinkable: false,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/qatar",
    Flag: "em-flag-qa",
    countryCode: "qa",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Romania",
    Drinkable: false,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/romania",
    Flag: "em-flag-ro",
    countryCode: "ro",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Russia",
    Drinkable: false,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/russia",
    Flag: "em-flag-ru",
    countryCode: "ru",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Rwanda",
    Drinkable: false,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/rwanda",
    Flag: "em-flag-rw",
    countryCode: "rw",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Réunion",
    Drinkable: true,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/reunion",
    Flag: "em-flag-re",
    countryCode: "re",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Saint Barthelemy",
    Drinkable: false,
    Warning: false,
    Source:
      "https://wwwnc.cdc.gov/travel/destinations/traveler/none/saint-barthelemy",
    Flag: "em-flag-bl",
    countryCode: "bl",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Saint Helena",
    Drinkable: true,
    Warning: false,
    Source:
      "https://wwwnc.cdc.gov/travel/destinations/traveler/none/saint-helena",
    Flag: "em-flag-sh",
    countryCode: "sh",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Saint Kitts and Nevis",
    Drinkable: false,
    Warning: false,
    Source:
      "https://wwwnc.cdc.gov/travel/destinations/traveler/none/st-kitts-and-nevis",
    Flag: "em-flag-kn",
    countryCode: "kn",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Saint Lucia",
    Drinkable: false,
    Warning: false,
    Source:
      "https://wwwnc.cdc.gov/travel/destinations/traveler/none/saint-lucia",
    Flag: "em-flag-lc",
    countryCode: "lc",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Saint Martin",
    Drinkable: false,
    Warning: false,
    Source:
      "https://wwwnc.cdc.gov/travel/destinations/traveler/none/saint-martin",
    Flag: "em-flag-mf",
    countryCode: "mf",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Saint Pierre and Miquelon",
    Drinkable: false,
    Warning: false,
    Source:
      "https://wwwnc.cdc.gov/travel/destinations/traveler/none/saint-pierre-and-miquelon",
    Flag: "em-flag-pm",
    countryCode: "pm",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Saint Vincent and the Grenadines",
    Drinkable: false,
    Warning: false,
    Source:
      "https://wwwnc.cdc.gov/travel/destinations/traveler/none/saint-vincent-and-the-grenadines",
    Flag: "em-flag-vc",
    countryCode: "vc",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Samoa",
    Drinkable: false,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/samoa",
    Flag: "em-flag-ws",
    countryCode: "ws",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "San Marino",
    Drinkable: true,
    Warning: false,
    Source:
      "https://wwwnc.cdc.gov/travel/destinations/traveler/none/san-marino",
    Flag: "em-flag-sm",
    countryCode: "sm",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Saudi Arabia",
    Drinkable: true,
    Warning: false,
    Source:
      "https://wwwnc.cdc.gov/travel/destinations/traveler/none/saudi-arabia",
    Flag: "em-flag-sa",
    countryCode: "sa",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Senegal",
    Drinkable: false,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/senegal",
    Flag: "em-flag-sn",
    countryCode: "sn",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Serbia",
    Drinkable: true,
    Warning: false,
    Source: "https://wikitravel.org/en/Serbia",
    Flag: "em-flag-rs",
    countryCode: "rs",
    shortSource: "https://wikitravel.org/"
  },
  {
    listCountry: "Seychelles",
    Drinkable: false,
    Warning: false,
    Source:
      "https://wwwnc.cdc.gov/travel/destinations/traveler/none/seychelles",
    Flag: "em-flag-sc",
    countryCode: "sc",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Sierra Leone",
    Drinkable: false,
    Warning: false,
    Source:
      "https://wwwnc.cdc.gov/travel/destinations/traveler/none/sierra-leone",
    Flag: "em-flag-sl",
    countryCode: "sl",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Singapore",
    Drinkable: true,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/singapore",
    Flag: "em-flag-sg",
    countryCode: "sg",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Sint Maarten",
    Drinkable: false,
    Warning: false,
    Source:
      "https://wwwnc.cdc.gov/travel/destinations/traveler/none/sint-maarten",
    Flag: "em-flag-sx",
    countryCode: "sx",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Slovakia",
    Drinkable: true,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/slovakia",
    Flag: "em-flag-sk",
    countryCode: "sk",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Slovenia",
    Drinkable: true,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/slovenia",
    Flag: "em-flag-si",
    countryCode: "si",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Solomon Islands",
    Drinkable: false,
    Warning: false,
    Source:
      "https://wwwnc.cdc.gov/travel/destinations/traveler/none/solomon-islands",
    Flag: "em-flag-sb",
    countryCode: "sb",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Somalia",
    Drinkable: false,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/somalia",
    Flag: "em-flag-so",
    countryCode: "so",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "South Africa",
    Drinkable: false,
    Warning: false,
    Source:
      "https://wwwnc.cdc.gov/travel/destinations/traveler/none/south-africa",
    Flag: "em-flag-za",
    countryCode: "za",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "South Georgia and the South Sandwich Islands",
    Drinkable: false,
    Warning: false,
    Source:
      "https://wwwnc.cdc.gov/travel/destinations/traveler/none/south-georgia-south-sandwich-islands",
    Flag: "em-flag-gs",
    countryCode: "gs",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "South Korea",
    Drinkable: true,
    Warning: false,
    Source:
      "https://wwwnc.cdc.gov/travel/destinations/traveler/none/south-korea",
    Flag: "em-flag-kr",
    countryCode: "kr",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "South Sudan",
    Drinkable: false,
    Warning: false,
    Source:
      "https://wwwnc.cdc.gov/travel/destinations/traveler/none/south-sudan",
    Flag: "em-flag-ss",
    countryCode: "ss",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Spain",
    Drinkable: true,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/spain",
    Flag: "em-flag-es",
    countryCode: "es",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Sri Lanka",
    Drinkable: false,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/sri-lanka",
    Flag: "em-flag-lk",
    countryCode: "lk",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Sudan",
    Drinkable: false,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/sudan",
    Flag: "em-flag-sd",
    countryCode: "sd",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Suriname",
    Drinkable: false,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/suriname",
    Flag: "em-flag-sr",
    countryCode: "sr",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Swaziland",
    Drinkable: false,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/eswatini",
    Flag: "em-flag-sz",
    countryCode: "sz",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Sweden",
    Drinkable: true,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/sweden",
    Flag: "em-flag-se",
    countryCode: "se",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Switzerland",
    Drinkable: true,
    Warning: false,
    Source:
      "https://wwwnc.cdc.gov/travel/destinations/traveler/none/switzerland",
    Flag: "em-flag-ch",
    countryCode: "ch",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Syria",
    Drinkable: false,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/syria",
    Flag: "em-flag-sy",
    countryCode: "sy",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "São Tomé and Príncipe",
    Drinkable: false,
    Warning: false,
    Source:
      "https://wwwnc.cdc.gov/travel/destinations/traveler/none/sao-tome-and-principe",
    Flag: "em-flag-st",
    countryCode: "st",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Taiwan",
    Drinkable: false,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/taiwan",
    Flag: "em-flag-tw",
    countryCode: "tw",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Tajikistan",
    Drinkable: false,
    Warning: false,
    Source:
      "https://wwwnc.cdc.gov/travel/destinations/traveler/none/tajikistan",
    Flag: "em-flag-tj",
    countryCode: "tj",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Tanzania",
    Drinkable: false,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/tanzania",
    Flag: "em-flag-tz",
    countryCode: "tz",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Thailand",
    Drinkable: false,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/thailand",
    Flag: "em-flag-th",
    countryCode: "th",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Timor-Leste",
    Drinkable: false,
    Warning: false,
    Source:
      "https://wwwnc.cdc.gov/travel/destinations/traveler/none/east-timor",
    Flag: "em-flag-tl",
    countryCode: "tl",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Tinian",
    Drinkable: false,
    Warning: false,
    Source:
      "https://wwwnc.cdc.gov/travel/destinations/traveler/none/northern-mariana-islands",
    Flag: "em-flag-mp",
    countryCode: "mp",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Togo",
    Drinkable: false,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/togo",
    Flag: "em-flag-tg",
    countryCode: "tg",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Tokelau",
    Drinkable: false,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/tokelau",
    Flag: "em-flag-tk",
    countryCode: "tk",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Tonga",
    Drinkable: false,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/tonga",
    Flag: "em-flag-to",
    countryCode: "to",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Trinidad and Tobago",
    Drinkable: false,
    Warning: false,
    Source:
      "https://wwwnc.cdc.gov/travel/destinations/traveler/none/trinidad-and-tobago",
    Flag: "em-flag-tt",
    countryCode: "tt",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Tunisia",
    Drinkable: false,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/tunisia",
    Flag: "em-flag-tn",
    countryCode: "tn",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Turkey",
    Drinkable: false,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/turkey",
    Flag: "em-flag-tr",
    countryCode: "tr",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Turkmenistan",
    Drinkable: false,
    Warning: false,
    Source:
      "https://wwwnc.cdc.gov/travel/destinations/traveler/none/turkmenistan",
    Flag: "em-flag-tm",
    countryCode: "tm",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Turks and Caicos Islands",
    Drinkable: false,
    Warning: false,
    Source:
      "https://wwwnc.cdc.gov/travel/destinations/traveler/none/turks-and-caicos",
    Flag: "em-flag-tc",
    countryCode: "tc",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Tuvalu",
    Drinkable: false,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/tuvalu",
    Flag: "em-flag-tv",
    countryCode: "tv",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Uganda",
    Drinkable: false,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/uganda",
    Flag: "em-flag-ug",
    countryCode: "ug",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Ukraine",
    Drinkable: false,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/ukraine",
    Flag: "em-flag-ua",
    countryCode: "ua",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "United Arab Emirates",
    Drinkable: true,
    Warning: false,
    Source:
      "https://wwwnc.cdc.gov/travel/destinations/traveler/none/united-arab-emirates",
    Flag: "em-flag-ae",
    countryCode: "ae",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "United States",
    Drinkable: true,
    Warning: false,
    Source:
      "https://wwwnc.cdc.gov/travel/destinations/traveler/none/united-states",
    Flag: "em-flag-us",
    countryCode: "us",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Uruguay",
    Drinkable: false,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/uruguay",
    Flag: "em-flag-uy",
    countryCode: "uy",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Uzbekistan",
    Drinkable: false,
    Warning: false,
    Source:
      "https://wwwnc.cdc.gov/travel/destinations/traveler/none/uzbekistan",
    Flag: "em-flag-uz",
    countryCode: "uz",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Vanuatu",
    Drinkable: false,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/vanuatu",
    Flag: "em-flag-vu",
    countryCode: "vu",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Vatican City",
    Drinkable: true,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/italy",
    Flag: "em-flag-va",
    countryCode: "va",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Venezuela",
    Drinkable: false,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/venezuela",
    Flag: "em-flag-ve",
    countryCode: "ve",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Vietnam",
    Drinkable: false,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/vietnam",
    Flag: "em-flag-vn",
    countryCode: "vn",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Virgin Islands, British",
    Drinkable: false,
    Warning: false,
    Source:
      "https://wwwnc.cdc.gov/travel/destinations/traveler/none/british-virgin-islands",
    Flag: "em-flag-vg",
    countryCode: "vg",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Virgin Islands, U.S.",
    Drinkable: true,
    Warning: false,
    Source:
      "https://wwwnc.cdc.gov/travel/destinations/traveler/none/usvirgin-islands",
    Flag: "em-flag-vi",
    countryCode: "vi",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Western Sahara",
    Drinkable: false,
    Warning: false,
    Source:
      "https://wwwnc.cdc.gov/travel/destinations/traveler/none/western-sahara",
    Flag: "em-flag-eh",
    countryCode: "eh",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Yemen",
    Drinkable: false,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/yemen",
    Flag: "em-flag-ye",
    countryCode: "ye",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Zambia",
    Drinkable: false,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/zambia",
    Flag: "em-flag-zm",
    countryCode: "zm",
    shortSource: "https://wwwnc.cdc.gov/travel"
  },
  {
    listCountry: "Zimbabwe",
    Drinkable: false,
    Warning: false,
    Source: "https://wwwnc.cdc.gov/travel/destinations/traveler/none/zimbabwe",
    Flag: "em-flag-zw",
    countryCode: "zw",
    shortSource: "https://wwwnc.cdc.gov/travel"
  }
];
